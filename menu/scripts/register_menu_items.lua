function onInit()		
	UDGCoreRPGEffectsManagerHelper.verbose({"register_menu_items.lua::onInit()"});
	registerMenuItems();		
end

function registerMenuItems() 	
	UDGCoreRPGEffectsManagerHelper.verbose({"register_menu_items.lua::registerMenuItems()"});

	OptionsManager.registerOption2("CORERPG_EFFECTS_MANAGER_DEBUG", false, "menu_option_header_corerpg_effects_manager", "menu_option_header_corerpg_effects_manager_debug", "option_entry_cycler", { labels = "option_val_off|option_val_debug|option_val_verbose", values = "off|debug|verbose", default = "off" });

end
