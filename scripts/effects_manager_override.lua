function onInit()
	UDGCoreRPGEffectsManagerHelper.verbose({"effects_manager_override.lua::onInit()"});

	originalAddEffect = EffectManager.addEffect;
	EffectManager.addEffect = addEffect;
end


function addEffect(sUser, sIdentity, nodeCT, rNewEffect, bShowMsg)
	UDGCoreRPGEffectsManagerHelper.verbose({"effects_manager_override.lua::addEffect()"});

	originalAddEffect(sUser, sIdentity, nodeCT, rNewEffect, bShowMsg)

end
