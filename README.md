# CoreRPG Effects Manager for FGU
A little diddy to make things mo betta when it comes to effects management.

[Download](https://gitlab.com/ccthiel/CoreRPG-Effects-Manager/-/jobs/artifacts/latest/file/CoreRPG-Effects-Manager.ext?job=build-extension) the latest version of the plugin!

[Gitlab repository](https://gitlab.com/ccthiel/CoreRPG-Effects-Manager)

[Fantasy Grounds Forum Thread]()

[Discord Channel]()

Proud member of the [Fantasy Grounds Unofficial Developer's Guild!](https://gitlab.com/fantasy-grounds-unofficial-developers-guild)

Much Thanks To:
Kelrugem, Celestian, damned, Moon Wizard, tahl_liadon for answering my many questions and providing code, graphics, guidance, etc.

Features:

Please see [Bug Reporting](https://gitlab.com/ccthiel/CoreRPG-Effects-Manager/-/blob/main/BUG_REPORT.md) when it comes to filing a bug. I'd absolutely weclome people reporting bugs (or even forking the repository and submitting a merge request!), but please do a bit of work on your part to make my life easier. Thank you! 

Want to become a member of the Fantasy Grounds Unofficial Developers Guild? [Join up!](https://discord.gg/yAXPgR8Bc8) It'll give you [developer](https://docs.gitlab.com/ee/user/permissions.html) access to this repository (and hopefully others soon!)
